#+TITLE:  Comparaison AoA ToA Rssi
#+AUTHOR: Xavier TOLZA
#+DATE: 2020-05-21
#+LANGUAGE: fr
# #+PROPERTY: header-args :eval never-export
#+SETUPFILE: https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup

* Comparaison
  On peut afficher les trois courbes de CDF. Pour ça on commence par
  récupérer les valeurs:
#+begin_src python :results output :session :exports both
from pyorg.config import cache_folder
from os.path import join
import pyorg as np

labels = "RSSI,RSSI deux étapes,ToA (simulé),AoA".split(",")
line_styles="-:--"
colors = "0012"
names = "rssi,rssi_lsqr,toa,aoa".split(",")
cdf = [np.load(join(cache_folder,f"dist_err_{i}.npy")) for i in names]
#+end_src

#+RESULTS:

#+begin_src python :results output file :exports both :session :var filename="comparison_cdf.png"
import pyorg as org
with org.Subplots(filename,1,1,export_svg=True) as (fig, ax):
     for err, label,ls,color in zip(cdf,labels,line_styles,colors):
          ax.plot(*np.get_cdf(err),label=label,ls=ls,color=f"C{color}")
     ax.legend()
     ax.set_info(*"Précision de localisation pour les trois métriques,Distance en mètres,Fonction de répartition de l'erreur de position".split(","))
     #+end_src

     #+RESULTS:
     [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/comparison_cdf.png]]


