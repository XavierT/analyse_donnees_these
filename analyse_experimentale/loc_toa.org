#+TITLE:  Analyse ToA dans les mesures de Loc
#+AUTHOR: Xavier TOLZA
#+DATE: 2020-05-21
#+LANGUAGE: fr
# #+PROPERTY: header-args :eval never-export

* TOA

** Extraction des mesures
 Tout d'abord, on extrait le ToA des mesures. Pour éviter d'avoir des
 grandeurs de temps trop grandes on réinitialise le compteur à chaque
 fichier de mesure (comme si la base de temps de la balise redémarrait
 à chaque fenêtre de mesure)

#+CALL: ./loc_extraction_mesures.org:data() :export none

#+RESULTS:
: 
: (15318, 20) (15318, 14, 16)


#+begin_src python :results output table :session :exports both
from pyorg import to_org
cols = "overflow,counter1,counter2,counter3,timestamp,tag_x,tag_y,ant_x,ant_y,transmitter_id,packet_counter".split(",")
data_toa = data.copy()[cols]
data_toa = data_toa.assign(tag_index=data_toa.groupby("tag_x,tag_y".split(",")).ngroup(),
                           ant_index=data_toa.groupby("ant_x,ant_y".split(",")).ngroup(),
                           filename_index=data.groupby("filename").ngroup()
)
groupby = data_toa.sort_values("timestamp").groupby("filename_index")
offset = groupby.first()[cols[:5]]
data_toa[cols[:4] + ["timestamp_rel"]] = data_toa[cols[:5]] - offset.loc[data_toa.filename_index.values].values

print(data_toa)
print(data.crc_correct.values.all())
 #+end_src

 #+RESULTS:
 #+begin_example
 overflow     counter1  ...  filename_index  timestamp_rel
 0           0.0          0.0  ...               0       0.000000
 1           0.0     319978.0  ...               0       0.012845
 2           0.0     639958.0  ...               0       0.032812
 3           0.0     959936.0  ...               0       0.052949
 4           0.0    1279916.0  ...               0       0.072898
 ...          ...  ...             ...            ...
 15313       0.0  159435248.0  ...               7       9.969866
 15314       0.0  159671250.0  ...               7       9.981380
 15315       0.0  159755230.0  ...               7       9.989792
 15316       0.0  159991232.0  ...               7      10.001373
 15317       0.0  160075214.0  ...               7      10.009778

 [15318 rows x 15 columns]
 True
 #+end_example

 On réinitialise également le compteur de paquet pour chaque
 transmetteur au début de chaque mesure et on évite les overflows:

#+begin_src python :results output table :session :exports both
groupby = data_toa.groupby("filename_index,transmitter_id".split(",")).packet_counter
counter = data_toa.packet_counter.values - groupby.first().iloc[groupby.ngroup()].values
overflow = 2**16
counter[counter<0] += overflow
data_toa.packet_counter = counter

print(to_org(data_toa.packet_counter.describe()))
 #+end_src

 #+RESULTS:
 | Index | packet_counter |
 |-------+---------------|
 | count |     15318.000 |
 | mean  |       221.155 |
 | std   |       144.709 |
 | min   |         0.000 |
 | 25%   |        94.000 |
 | 50%   |       207.000 |
 | 75%   |       341.000 |
 | max   |       500.000 |

 Puis, on extrait les valeurs de toa
 #+begin_src python :results output table :session :exports both
time = data_toa["timestamp"].values
time -= time.min()

timer_freq = 16e6
registers = data_toa[cols[:4]].values
overflow = registers[:,0]
counter = registers[:,1:].mean(1)
toa = counter/timer_freq + (overflow*2**32)/timer_freq
data_toa["toa"] = toa
print(to_org(data_toa.toa.describe()))
 #+end_src

 #+RESULTS:
 | Index |       toa |
 |-------+-----------|
 | count | 15318.000 |
 | mean  |     4.884 |
 | std   |     2.935 |
 | min   |     0.000 |
 | 25%   |     2.307 |
 | 50%   |     4.780 |
 | 75%   |     7.524 |
 | max   |    10.017 |

Au final, voilà les données de ToA:
#+begin_src python :results output :session :exports both
print(data_toa)
#+end_src

#+RESULTS:
#+begin_example
overflow     counter1  ...  timestamp_rel        toa
0           0.0          0.0  ...       0.000000   0.000000
1           0.0     319978.0  ...       0.012845   0.019999
2           0.0     639958.0  ...       0.032812   0.039997
3           0.0     959936.0  ...       0.052949   0.059996
4           0.0    1279916.0  ...       0.072898   0.079995
...          ...  ...            ...        ...
15313       0.0  159435248.0  ...       9.969866   9.964703
15314       0.0  159671250.0  ...       9.981380   9.979453
15315       0.0  159755230.0  ...       9.989792   9.984702
15316       0.0  159991232.0  ...      10.001373   9.999452
15317       0.0  160075214.0  ...      10.009778  10.004701

[15318 rows x 16 columns]
#+end_example

** Régression linéaire
   On affiche les valeurs mesurées:
  #+begin_src python :results output file :exports both :session :var filename="verif_toa.png"
import pyorg as org
with org.Subplots(filename,3,2,figsize=(10,10)) as (fig, axes):
     for (antenna_name,df_ant), ax in zip(data_toa.sort_values("timestamp").groupby("ant_index"),axes):
          for _,df in df_ant.groupby("tag_x,tag_y".split(",")):
               x = df.timestamp_rel.values
               label = f'Meas{df.ant_index.values[0]} & Trans{df_ant.iloc[0].transmitter_id}'
               ax[0].plot(x,df.toa.values,label=label)
               y = df.packet_counter.values
               ax[1].plot(x,y,label=label)
               ax[0].set_info(*"Valeurs de ToA mesurées,Temps (sec),ToA (sec)".split(","))
               ax[1].set_info(*"Compteur de paquet,Temps (sec),Valeur du compteur".split(","))
	#+end_src

	#+RESULTS:
	[[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/verif_toa.png]]

  Tout a l'air bien linéaire, on peut faire la régression:

 #+begin_src python :results output :session :exports input
from scipy.stats import linregress

N=data_toa.nunique()
AB=np.zeros((2,N.ant_index,N.tag_index))+np.nan
std=np.zeros(AB.shape[1:])+np.nan
filename_index=np.zeros(std.shape,dtype=np.int32)+np.nan

ant_c = np.empty((AB.shape[1],2))+np.nan
tag_c = np.empty((AB.shape[2],2))+np.nan
ant_name = [None for i in range(AB.shape[1])]
tag_name = [None for i in range(AB.shape[2])]


for i, df_ant in data_toa.groupby("ant_index"):
    ant_c[i, :] = df_ant.filter(regex="ant_(x|y)").values[0]
    ant_name[i] = df_ant.ant_index.values[0]
    for j,df_tag in df_ant.groupby("tag_index"):
        tag_c[j,:] = df_tag.filter(regex="tag_(x|y)").values[0]
        tag_name[j] = df_tag.transmitter_id.values[0]
        y = df_tag.toa.values
        x = df_tag.packet_counter.values.astype(np.uint32)
        linreg = linregress(x,y)
        AB[0,i,j], AB[1,i,j] = a, b = linreg.slope,linreg.intercept
        std[i,j] = np.std(y-a*x-b)
        filename_index[i,j] = df_tag.filename_index.values[0]
data_toa = data_toa.assign(**dict(zip("ab",AB[:,data_toa.ant_index,data_toa.tag_index])))
data_toa["err"] = np.abs(data_toa.toa.values-(data_toa.a*data_toa.packet_counter+data_toa.b).values)
data_toa.sort_values("ant_index,tag_index,packet_counter".split(",")).to_csv("/tmp/data_toa.csv")
 #+end_src

 #+RESULTS:

** Analyse de \nu
 On affiche A, B et la STD pour voir si il n'y a pas des valeurs
 anormales:
 #+begin_src python :results output file :exports both :session :var filename="ABstd.png"
from scipy.constants import speed_of_light as c
with org.Subplots(filename,1,3,figsize=(5,8),gridspec_kw=dict(wspace=3,left=0.1,right=0.85,bottom=0.05,top=0.96),sharex=True,sharey=True) as (fig, axes):
    import matplotlib.pyplot as plt
    _,caxes = plt.subplots(1,3,num=fig.number,gridspec_kw=dict(wspace=10,left=0.23,right=0.91,bottom=0.05,top=0.96))
    for im,ax,cax,title in zip(np.concatenate([AB,[std*c]],axis=0),axes,caxes,"A(sec),B(sec),std(m)".split(",")):
         imc = ax.imshow(im.T,origin="upper")
         plt.colorbar(imc,cax=cax)
         ax.set_info(title,"Receivers")
    axes[0].set_ylabel("Transmitters")
      #+end_src

      #+RESULTS:
      [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/ABstd.png]]


 Une STD < 50m, voyons voir si on affiche les résidus du linear
 fit. C'est à dire que si on a $ToA=a\times P_{id}+b + \nu$ on affiche $ToA-a\times
 P_id-b$:
 #+begin_src python :results output file :exports both :session :var filename="regr_lin_test.png"
import pyorg as org
from itertools import cycle
with org.Subplots(filename,3,5,figsize=(16,8),sharex=True) as (fig, axes):
     for (ant_i,df_ant), _ax in zip(data_toa.sort_values("timestamp").groupby("ant_index"),axes):
          for (tag_i, df), ax in zip(df_ant.groupby("tag_index"),cycle(_ax)):
               x = df.packet_counter.values
               a,b = AB[:,ant_i,tag_i]
               y = df.toa.values - a*x-b
               ax.plot(x,y*c,label=label)
               ax.set_info(df_ant.ant_index.values[0],"Paquets","Erreur de\nrégression (m)")
               ax.set_ylim(-60,60)
 #+end_src

 #+RESULTS:
 [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/regr_lin_test.png]]



 On affiche maintenant la statistique de l'erreur et on y superpose le
 modèle du bruit de mesure:
 #+begin_src python :results output file :exports both :session :var filename="stat_toa_linreg_error.png"
import pyorg as org
a,b = AB[:,data_toa.ant_index.values,data_toa.tag_index.values]
toa_err = data_toa.toa.values-a*data_toa.packet_counter.values-b
data_toa["err"] = toa_err
with org.Subplots(filename,1,1) as (fig, ax):
     ax.hist(toa_err*c,bins=np.linspace(-75,75,100),density=True)
     ax.set_info(*"Statistique d'erreur de la régression linéaire ToA,Erreur de régression linéaire,PDF de l'erreur".split(","))
     ax.set_xlim(-100,100)
      #+end_src

      #+RESULTS:
      [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/stat_toa_linreg_error.png]]

 
 On va essayer de faire coller un modèle à ça: un bruit gaussien + une
 quantif + un bruit gaussien. On sait que la quantification se fait à
 8MHz, et le premier bruit gaussien est à $\sigma_1=4.7m$. On cherche la
 valeur de $\sigma_2$, pour ça on va faire un LSM error sur la CDF:
 #+begin_src python :results output :session :exports both
from scipy.interpolate import interp1d
import numpy as _np

_x = np.sort(toa_err)*c
_x = _x[~np.isnan(_x)]
xmax=int(np.floor(min(_x.max(),-_x.min())))
x,xstep = np.linspace(-xmax,xmax,xmax*2+1,retstep=True)
y = interp1d(_x,np.linspace(0,1,_x.size))(x)
sigma_search, sigma_search_step = np.linspace(8,15,50,retstep=True)

def gaussienne(x, sigma):
    return np.exp(-0.5*(x/sigma)**2)/(sigma*np.sqrt(2*np.pi))

Q=c/8e6
sigma1 = 4.7
quantif = (_np.abs(x)<Q/2)/(Q)
quantif /= quantif.sum(0)*xstep
mesure = gaussienne(x,sigma1)
model_meas = np.convolve(mesure,quantif, mode="same")
multipath=gaussienne(x[:,None],sigma_search[None,:])
model = np.apply_along_axis(lambda x: np.convolve(model_meas,x,mode="same"),axis=0,arr=multipath)
model = np.cumsum(model*xstep,axis=0)
print(model.shape)
 #+end_src

 #+RESULTS:
 : (291, 50)

 On visualise le fitting:

 #+begin_src python :results file output :exports both :session :var filename="tmp.png"
import pyorg as org
with org.Subplots(filename,1,1) as (fig, ax):
    ax.plot(x,np.cumsum(model_meas*xstep),label="Measurement noise CDF")
    ax.plot(_x,np.linspace(0,1,_x.size),color='r',label="Experimental CDF")
    ax.plot(x,model,color='k',alpha=0.1)
    ax.set_xlim(-100,100)
    ax.legend()
      #+end_src

      #+RESULTS:
      [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/tmp.png]]

 Et le coût du moindre carré:

 #+begin_src python :results output file :exports both :session :var filename="fit_sigma2.png"
import pyorg as org
with org.Subplots(filename,1,1) as (fig, ax):
    cost = np.linalg.norm(y[:,None]-model,axis=0)
    argmin = np.argmin_interpolated(cost)
    sigma2 = argmin*sigma_search_step+sigma_search.min()
    ax.plot(sigma_search, cost)
    ax.text(11,0.175,"$\sigma_2=%.3f$"%sigma2)
    ax.axvline(sigma2,ls=":",color='k',alpha=.5)
    ax.set_info(*"Recherche de $\sigma_2$,$\sigma_2$ (m),LMS cost".split(","))
      #+end_src

      #+RESULTS:
      [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/fit_sigma2.png]]

 On affiche le fit sur l'histogramme de tout à l'heure

 #+begin_src python :results file output :exports both :session :var filename="pdf_erreur_avec_modele.png"
with org.Subplots(filename,2,1) as (fig, ax):
    model = np.convolve(model_meas,gaussienne(x,sigma2),mode="same")
    _y = np.linspace(0,1,_x.size)
    ax[0].plot(x,np.cumsum(model_meas*xstep),label="Erreur mesure")
    ax[0].plot(_x,_y,label="Données réelles")
    ax[0].plot(x,np.cumsum(model*xstep),label="Fit trouvé",color='k',ls=':')
    ax[0].set_info(*"Statistique d'erreur de la régression linéaire ToA,Erreur de régression linéaire,CDF de l'erreur".split(","))
    ax[1].hist(toa_err*c,bins=np.linspace(-100,100,100),density=True)
    ax[1].plot(x,model,label="Modele",color="r")
    ax[1].plot(x,model_meas,label="Modele bruit mesure",color='k',alpha=.3)
    ax[1].set_info(*"Statistique d'erreur de la régression linéaire ToA,Erreur de régression linéaire,PDF de l'erreur".split(","))
    for i in ax:
        i.legend()
        i.set_xlim(-100,100)
     
 #+end_src

 #+RESULTS:
 [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/pdf_erreur_avec_modele.png]]


 Le modèle de bruit est plutôt correct. On refait la même courbe avec
 un export pour la thèse:
#+begin_src python :results output :session :exports both :var filename="toa_modele_bruit_loc.png"
with org.Subplots(filename,1,1,tight_layout=False,
                  gridspec_kw=dict(bottom=.24),
                  figsize=(6,6),export_svg=True) as (fig, ax):
    ax.hist(toa_err*speed_of_light,bins=np.linspace(-100,100,100),density=True)
    ax.plot(x,model,label="Modele canal variant",color="k")
    ax.plot(x,model_meas,label="Modele en conduit",color='k',ls=':')
    ax.set_info("Statistique de $\\nu^{(j,i,n)}$ (ToA)",
                "$\\nu^{(j,i,n)} \\times c_0$ (en mètres)",
                "Densité de probabilité")

    axx = ax.twinx()
    ax2 = axx.twiny()
    ax2.plot(*np.get_cdf(toa_err*1e6),color="r",label="Mesures expérimentales")
    ax2.plot(1e6*x/speed_of_light,np.cumsum(model),color="C8",label="Modèle canal variant")
    ax2.plot(1e6*x/speed_of_light,np.cumsum(model_meas),color="C8",ls='--',label="Modèle en conduit")
    ax2.set_xlabel("$\\nu^{(j,i,n)}$ (en $\\mu s$)")
    ax2.set_ylim(0,1)
    ax2.set_yticks(np.linspace(0,1,7))
    ax2.xaxis.set_major_formatter(plt.FuncFormatter(lambda x,y: "%.2f"%x))
    ax2.yaxis.set_major_formatter(plt.FuncFormatter(lambda x,y: "%.2f"%x))
    axx.set_ylabel("Fonction de répartition",color='r')
    axx.tick_params(axis='y', colors='r')
    ax2.legend(loc=(0.53,-0.35))

    ax.legend(loc=(0,-0.299))
    ax.set_xlim(-60,60)
    ax.set_ylim(0,0.03)
    xlim2 = np.array([1e6*-60/speed_of_light,1e6*60/speed_of_light])
    ax2.set_xlim(xlim2)
    ax2.set_xticks(np.linspace(*xlim2,7))
#+end_src

#+RESULTS:
: /home/xavier/PycharmProjects/analyse_donnees_these/img/toa_modele_bruit_loc.png

** Simulation pour localisation
 Maintenant on va faire une simulation de toa avec le profil de bruit
 qu'on a mesuré. On commence par trouver nos alpha et tau pour la
 simulation. 
#+begin_src python :results output :session :exports input
from pytoa.tools import split_alpha_tau
J,I = AB.shape[1:]
alpha=np.random.normal(1, 0, (J+I))
tau = np.random.uniform(-1,1,J+I)*0#1e-3
(alphaj,alphai),(tauj,taui) = split_alpha_tau(alpha,tau,J)
#+end_src

#+RESULTS:

On calcule aussi le temps de propagation et on en déduit les valeurs
de toa:
 #+begin_src python :results output :session :exports both
from pytoa.simulation import simulate_toa
sim = simulate_toa(tag_c[:,:2],ant_c[:,:2],50)
 #+end_src

 #+RESULTS:

On génère du bruit avec le profil de bruit trouvé précédemment:
#+begin_src python :results output :session :exports input
bruit_quantif = np.random.uniform(-Q/2,Q/2,sim.tr.shape)
bruit_mes = np.random.normal(0,sigma1,sim.tr.shape)
bruit_mp = np.random.normal(0,sigma2,sim.tr.shape)
bruit_tr = bruit_quantif + bruit_mes + bruit_mp
#+end_src

#+RESULTS:


Et on vérifie que ce bruit a une bonne densité de proba:
#+begin_src python :results output file :exports both :session :var filename="verif_bruit_tr.png"
with org.Subplots(filename,1,1) as (fig, ax):
     ax.hist(bruit_tr.ravel(),bins=100,density=True)
     model_x,model_x_step = np.linspace(-50,50,1000,retstep=True)
     model = (np.abs(model_x)<(Q/2))/Q
     model = np.convolve(model,np.gaussian(model_x,sigma1),mode="same")*model_x_step
     model = np.convolve(model,np.gaussian(model_x,sigma2),mode="same")*model_x_step
     ax.plot(model_x,model,label="Modele")
     ax.set_info(*"CDF du bruit généré,Bruit en m,CDF".split(","))
     #+end_src

     #+RESULTS:
     [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/verif_bruit_tr.png]]

On colle bien au modèle, on ajoute maintenant le bruit à tr:
#+begin_src python :results output :session :exports both
from pymath.constants import speed_of_light
trn = sim.tr+bruit_tr/speed_of_light
print(trn.shape)
#+end_src

#+RESULTS:
: (3, 48, 50)


On fait une régression linéaire
#+begin_src python :results output :session :exports both
AB = np.linregress(sim.te,trn,axis=-1)
print(AB.shape)
#+end_src

#+RESULTS:
: (2, 3, 48)

Et on fait une résolution de la position par ToA:

#+begin_src python :results output :session :exports input
from pytoa.AB import toa_map
indexes = np.arange(len(tag_c))
#np.random.shuffle(indexes)
ancre = indexes[0]
resolution=0.5
maps = np.array([toa_map(AB[:, :, [ancre, tag]],
                         ant_c[:, :2],
                         tag_c[[ancre], :2],
                         [-15, 12],
                         resolution)
                 for tag in indexes[1:]])
print(maps.shape)
found_pos = np.argmin2d(maps)*resolution-np.array([15,0])[None]
#+end_src

#+RESULTS:
: (47, 30, 24)


On peut maintenant afficher quelques exemples aléatoires de cartes:

#+begin_src python :results output file :exports both :session :var filename="toa_map_examples.png"
with org.Subplots(filename,3,3) as (fig, axes):
     for ax,map,fp,tag in zip(axes.ravel(),maps,found_pos,indexes[1:]):
          clim = (np.nanmin(map),np.nanmedian(map))
          ax.imshow(map.T,origin="lower",clim=clim,interpolation="bicubic",extent=[-15,0,0,12])
          ax.scatter(*ant_c[:,:2].T,marker='s',color='k',label="Balises")
          ax.scatter(*tag_c[tag,:2].T,marker='s',color='r',label="Tag")
          ax.scatter(*tag_c[ancre,:2].T,color='r',label="Tag")
          ax.scatter(*fp,color='g',label="Found")
     axes[0,0].legend()
#+end_src

     #+RESULTS:
     [[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/toa_map_examples.png]]


On mesure l'erreur de distance:
#+begin_src python :results output :exports both :session
from pyorg.cache import *
import pyorg as np
with NumpyCache(name="dist_err_toa") as (c, dist_err_toa):
    if dist_err_toa is None:
        dist_err_toa = np.distance(tag_c[1:],found_pos)
        c(dist_err_toa)
print(dist_err_toa)
#+end_src

#+RESULTS:
: [ 6.8224629   2.57670332  3.84002604  4.41208567  5.00092991  3.01039864
:   7.8152991   7.28598655  7.15949719  2.55566038  3.73065678  4.39323343
:   2.94334843  3.61332257  3.82001309  7.45829069 10.2429732   3.09142362
:   6.55296116  6.05405649  6.21642984  6.04438582  3.39249171  7.83600026
:   7.3610665   5.17475603  4.74473392  8.71148667  9.77304456  2.32107734
:   5.08348306  2.28324331  5.76419986  5.4886246   5.71945802  2.21939181
:   2.38296034  6.06337365  2.59069489  3.92734007  4.63862048  4.02613959
:   6.98977825  2.60155723  3.06282549  3.94726488 10.24832181]


Et on affiche la CDF:

#+begin_src python :results output file :exports both :session :var filename="img/toa_dist_cdf.png"
with org.Subplots(filename,1,1) as (fig, ax):
     ax.plot(*np.get_cdf(dist_err_toa))
     ax.set_info(*"Erreur de positionnement par ToA,Distance en mètres,CDF de l'erreur".split(","))
#+end_src

#+RESULTS:
[[file:/home/xavier/PycharmProjects/analyse_donnees_these/img/img/toa_dist_cdf.png]]

Les informations de stat sur l'erreur:
#+begin_src python :results output table :session :exports both
dist_err_toa_stat = np.Series(dist_err_toa).describe(percentiles=[0.8]).iloc[1:]
print(np.to_org(dist_err_toa_stat))
#+end_src

#+RESULTS:
| Index |   None |
|-------+--------|
| mean  |  5.127 |
| std   |  2.213 |
| min   |  2.219 |
| 50%   |  4.745 |
| 80%   |  7.126 |
| max   | 10.248 |
